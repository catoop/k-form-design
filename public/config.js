
const setConfig = {
    posSelect: {
        url: serverconfig.BASE_URL + "/muses-gateway/api/upms/pos/getPosList",
        label: 'posName',
        value: 'id',
    },
    rolSelect: {
        url: serverconfig.BASE_URL + "/muses-gateway/api/upms/role/detail/query",
        label: 'roleName',
        value: 'id',
    },
    // groSelect: {
    //     url: serverconfig.BASE_URL + "/muses-gateway/api/upms/group/detail/query",
    //     label: 'groupName',
    //     value: 'id',
    // },
    orgSelect: {
        url: serverconfig.BASE_URL + "/muses-gateway/api/upms/org/getOrgTree?onlyOrg=false",
        key: 'orgCode',
        title: 'orgName',
        value: 'id',
        children: 'children',
        treeDefaultExpandAll: false,
        multiple: true,
        maxHeight: '70vh',
    },
    userSelectPanel: {
        url: serverconfig.BASE_URL + "/muses-gateway/api/upms/user/page",
        columns: [
            {
                title: "账号",
                dataIndex: "account",
                width: "25%",
            },
            {
                title: "姓名",
                dataIndex: "name",
                ellipsis: true,
                width: "25%",
            },
            {
                title: "性别",
                dataIndex: "sex",
                scopedSlots: { customRender: 'sex' },
                width: "25%",
            },
            {
                title: "手机号",
                dataIndex: "phone",
                width: "25%",
            },
        ]


    }
}

export  {setConfig}