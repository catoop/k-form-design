/*
 * @Author: your name
 * @Date: 2021-04-14 15:21:02
 * @LastEditTime: 2021-05-26 13:20:08
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \k-form-design\public\API_Config.js
 */
const API_Config = {
    posSelect: {
        methods: "get",
        paramsType: "parmas",
        url: serverconfig.BASE_URL + "/muses-gateway/api/upms/pos/getPosList",
        label: 'posName',
        value: 'id',
    },
    rolSelect: {
        methods: "post",
        paramsType: "data",
        url: serverconfig.BASE_URL + "/muses-gateway/api/upms/role/detail/query",
        label: 'roleName',
        value: 'id',
        params: { delFlag: 1 },
    },
    groSelect: {
        methods: "post",
        paramsType: "data",
        url: serverconfig.BASE_URL + "/muses-gateway/api/upms/group/detail/query",
        label: 'groupName',
        value: 'id',
        params: { delFlag: 1 },
    },
    orgSelect: {
        methods: "get",
        paramsType: "params",
        url: serverconfig.BASE_URL + "/muses-gateway/api/upms/org/getOrgTree?onlyOrg=false",
        key: 'orgCode',
        title: 'orgName',
        value: 'id',
        children: 'children',
        treeDefaultExpandAll: false,
        multiple: true,
        maxHeight: '70vh',
        label: 'orgName'
    },
    dict: {
        url: serverconfig.BASE_URL + "/muses-gateway/api/upms/dict/queryDictInfo",
        methods: "post",
        paramsType: "data",
        params: {
            delFlag: 1,
            dictKey: "processGroup",
            dictType: 2
        },
        label: "dictName",
        value: "id"
    },
    role: {
        url: serverconfig.BASE_URL + "/muses-gateway/api/upms/role/queryAllRoleInfoListByUserInfo",
        methods: "post",
        paramsType: "data",
        params: {
        },
        label: 'roleName',
        value: "id"
    },
    dynamicTable: {
        url: serverconfig.BASE_URL + "/muses-gateway/api/upms/user/page",
        methods: "post",
        paramsType: "data",
        params: {
        },
        label: 'roleName',
        value: "id"
    },
}