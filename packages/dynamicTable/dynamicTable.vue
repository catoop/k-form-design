<template>
  <a-table
    ref="table"
    bordered
    :columns="columns"
    :pagination="paginationConfig"
    :data-source="data"
    @change="handleTableChange"
  >
    <template slot="sex" slot-scope="text, record, index">
      <span>{{ text == 1 ? "男" : text == 2 ? "女" : "无" }}</span>
    </template>
  </a-table>
</template>

<script>
import axios from "axios";
import { setConfig } from "../../public/config";
export default {
  name: "KDynamicTable",
  props: [
    "record",
    "value",
    "config",
    "disabled",
    "columns",
    "dataTable",
    "optionsValue",
    "dynamicTableKey",
  ],
  data() {
    return {
      data: [],
      page: 1,
      pagesize: 10,
      paginationConfig: {
        position: "bottom",
        pageSizeOptions: ["10", "20", "30", "40"],
        showSizeChanger: true,
        defaultPageSize: 10,
        showQuickJumper: true,
        total: 0,
        "show-total": (total) => `共 ${total} 页`,
      },
    };
  },
  computed: {
    getTableData() {
      return this.data;
    },
  },
  watch: {
    dynamicTableKey: {
      handler() {
        if (API_Config[this.dynamicTableKey]) {
          axios({
            method: "post",
            url: API_Config[this.dynamicTableKey].url,
            data: {
              pageNum: this.page,
              pageSize: this.pagesize,
            },
            headers: {
              token: window.sessionStorage.user
                ? (window.sessionStorage.user + "").replace(/^\"|\"$/g, "")
                : "",
              "Content-Type": "application/json;charset=UTF-8",
            },
          }).then((res) => {
            this.paginationConfig.total = JSON.parse(res.data.data.total);
            res.data.data.list.map((v) => {
              v.key = v.id;
            });
            this.data = res.data.data.list;
          });
        }
      },
      immediate: true,
    },
  },
  methods: {
    getList() {
      axios({
        method: "post",
        url: API_Config[this.dynamicTableKey].url,
        data: {
          pageNum: this.page,
          pageSize: this.pagesize,
        },
        headers: {
          token: window.sessionStorage.user
            ? (window.sessionStorage.user + "").replace(/^\"|\"$/g, "")
            : "",
          "Content-Type": "application/json;charset=UTF-8",
        },
      }).then((res) => {
        this.data = res.data.data.list;
        this.paginationConfig.total = JSON.parse(res.data.data.total);
      });
    },
    // 切换页码和pageSize时触发
    handleTableChange(pagination, filters, sorter) {
      this.page = pagination.current;
      this.pagesize = pagination.pageSize;
      this.getList();
    },
    clear() {
      this.keywords = "";
    },
    query() {
      this.page = 1;
      this.pagesize = 10;
      axios({
        method: "post",
        url: setConfig.userSelectPanel.url,
        data: {
          pageNum: this.page,
          pageSize: this.pagesize,
          keyWord: this.keywords,
        },
        headers: {
          token: window.sessionStorage.user
            ? (window.sessionStorage.user + "").replace(/^\"|\"$/g, "")
            : "",
          "Content-Type": "application/json;charset=UTF-8",
        },
      }).then((res) => {
        this.data = res.data.data.list;
        this.paginationConfig.total = JSON.parse(res.data.data.total);
      });
    },
    setFileList() {
      // 当传入value改变时，fileList也要改变
      // 如果传入的值为字符串，则转成json

      this.echoData = this.value;
      if (this.disabled) {
        this.isdisabled = true;
      }
      this.selectedUsers = this.echoData[0].selectedUsers;
      this.selectedRowKeys = this.echoData[0].selectedRowKeys;
      // this.selectedRows = this.echoData[0].selectedRows;
      // this.finalSelectedRows = this.echoData[0].finalSelectedRows;
      axios({
        method: "post",
        url:
          serverconfig.BASE_URL + "/muses-gateway/api/upms/user/queryUserByIds",
        data: {
          ids: this.selectedRowKeys,
        },
        headers: {
          token: window.sessionStorage.user
            ? (window.sessionStorage.user + "").replace(/^\"|\"$/g, "")
            : "",
          "Content-Type": "application/json;charset=UTF-8",
        },
      }).then((res) => {
        this.finalSelectedRows = res.data.data;
        this.paginationConfigSelected.total = this.finalSelectedRows.length;
      });
      // 将转好的json覆盖组件默认值的字符串
      // this.handleSelectChange();
    },
    // 选择复选框时触发
    onSelectChange(selectedRowKeys, selectedRows) {
      this.selectedRowKeys = selectedRowKeys;
      this.selectedRows.push(...selectedRows);
    },
    // 点击选择用户时 打开弹窗
    openUserPanel() {
      this.visible = true;
      if (!this.disabled) {
        this.getUser();
      }
    },
    // 拉去用户列表数据
    getUser() {
      axios({
        method: "post",
        url: setConfig.userSelectPanel.url,
        data: {
          pageNum: this.page,
          pageSize: this.pagesize,
        },
        headers: {
          token: window.sessionStorage.user
            ? (window.sessionStorage.user + "").replace(/^\"|\"$/g, "")
            : "",
          "Content-Type": "application/json;charset=UTF-8",
        },
      }).then((res) => {
        this.data = res.data.data.list;
        this.paginationConfig.total = JSON.parse(res.data.data.total);
      });
    },
    // 切换页码和pageSize时触发
    handleTableChange(pagination, filters, sorter) {
      console.log(pagination);
      this.page = pagination.current;
      this.pagesize = pagination.pageSize;
      this.getUser();
    },
    // 弹窗确认触发
    handleOk() {
      this.visible = false;
      this.selectedUsers = "";
      this.finalSelectedRows = [];
      for (let index in this.selectedRowKeys) {
        for (let jndex in this.selectedRows) {
          if (this.selectedRowKeys[index] == this.selectedRows[jndex].id) {
            this.selectedUsers += this.selectedRows[jndex].name + ",";
            this.finalSelectedRows.push(this.selectedRows[jndex]);
            break;
          }
        }
      }
      this.selectedUsers = this.selectedUsers.substring(
        0,
        this.selectedUsers.length - 1
      );
      this.$emit("change", [
        {
          selectedUsers: this.selectedUsers,
          selectedRowKeys: this.selectedRowKeys,
          // selectedRows: this.selectedRows,
          // finalSelectedRows: this.finalSelectedRows,
        },
      ]);
      this.$emit("input", [
        {
          selectedUsers: this.selectedUsers,
          selectedRowKeys: this.selectedRowKeys,
          // selectedRows: this.selectedRows,
          // finalSelectedRows: this.finalSelectedRows,
        },
      ]);
    },
    // 弹框取消触发
    handleCancel() {
      this.visible = false;
    },
  },
};
</script>
<style lang="less" scoped>
/deep/ .ant-table-thead > tr > th,
/deep/.ant-table-tbody > tr > td {
  padding: 9px 16px;
}
</style>