/*
* author kcz
* date 2019-11-20
* description 表单控件项
*/
// 基础控件
function getNextDate(date, day) {
  var str = date.split(' ')[0]
  var arr = str.split('/')

  var dd = new Date(str);
  // var dd = new Date(arr[0].slice(1,-1)+'/'+arr[1].slice(1,-1)+'/'+arr[2].slice(1,-2));
  var hms = date.split(' ')[1]
  dd.setDate(dd.getDate() + day);
  var y = dd.getFullYear();
  var m = dd.getMonth() + 1 < 10 ? "0" + (dd.getMonth() + 1) : dd.getMonth() + 1;
  var d = dd.getDate() < 10 ? "0" + dd.getDate() : dd.getDate();
  return y + "-" + m + "-" + d + ' ' + hms;
};
import moment from 'moment'
let timer = new Date().toLocaleString({ hour12: false })
let timer1 = new Date();
timer1 = timer1.getHours()+':'+timer1.getMinutes()+':'+timer1.getSeconds()
timer = timer.replace(/上午|下午/g, ' ')
timer = timer.replace(/年|月/g, '/')
timer = timer.replace(/日/g, '')
timer = timer.split(' ')
// console.log(timer);
timer[1]  = timer1
timer = timer.join(' ')
// console.log(timer);
timer = getNextDate(timer, 0)
const setConfig = {
  posSelect: {
    url: serverconfig.BASE_URL + "/muses-gateway/api/upms/pos/getPosList",
    label: 'posName',
    value: 'id',
  },
  rolSelect: {
    url: serverconfig.BASE_URL + "/muses-gateway/api/upms/role/detail/query",
    label: 'roleName',
    value: 'id',
  },
  groSelect: {
    url: serverconfig.BASE_URL + "/muses-gateway/api/upms/group/detail/query",
    label: 'groupName',
    value: 'id',
  },
  orgSelect: {
    url: serverconfig.BASE_URL + "/muses-gateway/api/upms/org/getOrgTree?onlyOrg=false",
    key: 'orgCode',
    title: 'orgName',
    value: 'id',
    children: 'children',
    treeDefaultExpandAll: false,
    multiple: true,
    maxHeight: '70vh',
  },
  userSelectPanel: {
    url: serverconfig.BASE_URL + "/muses-gateway/api/upms/user/page",
    columns: [
      {
        title: "账号",
        dataIndex: "account",
        width: "25%",
      },
      {
        title: "姓名",
        dataIndex: "name",
        ellipsis: true,
        width: "25%",
      },
      {
        title: "性别",
        dataIndex: "sex",
        scopedSlots: { customRender: 'sex' },
        width: "25%",
      },
      {
        title: "手机号",
        dataIndex: "phone",
        width: "25%",
      },
    ]


  }
}
let urlPos, urlRol, urlGro, urlOrg
urlPos = setConfig.posSelect.url
urlRol = setConfig.rolSelect.url
urlGro = setConfig.groSelect.url
urlOrg = setConfig.orgSelect.url
export const basicsList = [
  {
    type: "input", // 表单类型
    label: "输入框", // 标题文字
    icon: "icon-write",
    blindComponentsNumber: [{ id: undefined, value: '1', bindValue: undefined }],
    decOpacity: false,
    options: {
      type: "text",
      width: "100%", // 宽度
      defaultValue: "", // 默认值
      placeholder: "请输入", // 没有输入时，提示文字
      clearable: true,
      maxLength: null,
      hidden: false, // 是否隐藏，false显示，true隐藏
      newHidden: false, isShow: true,
      selected: '',
      selectedValue: '',
      disabled: false, // 是否禁用，false不禁用，true禁用
      queryList: true, // 是否被查询列表获取
      print: false, // 是否可被打印
      isCondition: false,
    },
    labelCol: { span: 4 },
    wrapperCol: { span: 18 },
    model: "", // 数据字段
    key: "",
    rules: [
      //验证规则
      {
        required: false, // 必须填写
        message: "必填项"
      }
    ]
  },
  {
    type: "textarea", // 表单类型
    label: "文本框", // 标题文字
    icon: "icon-edit",
    blindComponentsNumber: [{ id: undefined, value: '1', bindValue: undefined }],
    decOpacity: false, options: {
      width: "100%", // 宽度
      minRows: 4,
      maxRows: 6,
      maxLength: null,
      defaultValue: "",
      clearable: false,
      hidden: false, // 是否隐藏，false显示，true隐藏
      newHidden: false, isShow: true,
      selected: '',
      selectedValue: '',
      disabled: false,
      queryList: true, // 是否被查询列表获取
      print: false, // 是否可被打印
      placeholder: "请输入"
    },
    labelCol: { span: 4 },
    wrapperCol: { span: 18 },
    model: "", // 数据字段
    key: "",
    rules: [
      {
        required: false,
        message: "必填项",
        trigger: ['blur', 'change']
      }
    ]
  },
  {
    blindComponentsNumber: [{ id: undefined, value: '1', bindValue: undefined }],
    type: "number", // 表单类型
    label: "数字输入框", // 标题文字
    icon: "icon-number",
    decOpacity: false, options: {
      isCondition: false,
      width: "100%", // 宽度
      defaultValue: 0, // 默认值
      min: null, // 可输入最小值
      max: null, // 可输入最大值
      precision: null,
      step: 1, // 步长，点击加减按钮时候，加减多少
      hidden: false, // 是否隐藏，false显示，true隐藏
      newHidden: false, isShow: true,
      selected: '',
      selectedValue: '',
      disabled: false, //是否禁用
      queryList: true, // 是否被查询列表获取
      print: false, // 是否可被打印
      placeholder: "请输入"
    },
    labelCol: { span: 4 },
    wrapperCol: { span: 18 },
    model: "", // 数据字段
    key: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    blindComponentsNumber: [{ id: undefined, value: '1', bindValue: undefined }],
    type: "select", // 表单类型
    label: "下拉选择器", // 标题文字
    icon: "icon-xiala",
    blindComponentKey: [],
    addComponentKey: [],
    decOpacity: false, options: {
      isCondition: false,
      width: "100%", // 宽度
      defaultValue: undefined, // 下拉选框请使用undefined为默认值
      multiple: false, // 是否允许多选
      disabled: false, // 是否禁用
      clearable: false, // 是否显示清除按钮
      hidden: false, // 是否隐藏，false显示，true隐藏
      newHidden: false, isShow: true,
      selected: '',
      selectedValue: '',
      placeholder: "请选择", // 默认提示文字
      dynamicKey: "",
      dynamic: false,
      decOpacity: false, options: [
        // 下拉选择项配置
        {
          value: "1",
          label: "下拉框1"
        },
        {
          value: "2",
          label: "下拉框2"
        }
      ],
      queryList: true, // 是否被查询列表获取
      print: false, // 是否可被打印
      showSearch: false // 是否显示搜索框，搜索选择的项的值，而不是文字
    },
    labelCol: { span: 4 },
    wrapperCol: { span: 18 },
    model: "",
    key: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    blindComponentsNumber: [{ id: undefined, value: '1', bindValue: undefined }],
    type: "checkbox",
    label: "多选框",
    icon: "icon-duoxuan1",
    blindComponentKey: [],
    addComponentKey: [],
    decOpacity: false, options: {
      isCondition: false,
      disabled: false, //是否禁用
      hidden: false, // 是否隐藏，false显示，true隐藏
      newHidden: false, isShow: true,
      selected: '',
      selectedValue: '',
      defaultValue: [],
      dynamicKey: "",
      dynamic: false,
      queryList: true, // 是否被查询列表获取
      print: false, // 是否可被打印
      decOpacity: false, options: [
        {
          value: "1",
          label: "选项1"
        },
        {
          value: "2",
          label: "选项2"
        },
        {
          value: "3",
          label: "选项3"
        }
      ]
    },
    labelCol: { span: 4 },
    wrapperCol: { span: 18 },
    model: "",
    key: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    blindComponentsNumber: [{ id: undefined, value: '1', bindValue: undefined }],
    type: "radio", // 表单类型
    label: "单选框", // 标题文字
    icon: "icon-danxuan-cuxiantiao",
    blindComponentKey: [],
    addComponentKey: [],
    decOpacity: false, options: {
      isCondition: false,
      disabled: false, //是否禁用
      hidden: false, // 是否隐藏，false显示，true隐藏
      newHidden: false, isShow: true,
      selected: '',
      selectedValue: '',
      defaultValue: "", // 默认值
      dynamicKey: "",
      dynamic: false,
      queryList: true, // 是否被查询列表获取
      print: false, // 是否可被打印
      decOpacity: false, options: [
        {
          value: "1",
          label: "选项1"
        },
        {
          value: "2",
          label: "选项2"
        },
        {
          value: "3",
          label: "选项3"
        }
      ]
    },
    labelCol: { span: 4 },
    wrapperCol: { span: 18 },
    model: "",
    key: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    blindComponentsNumber: [{ id: undefined, value: '1', bindValue: undefined }],
    type: "date", // 表单类型
    label: "日期选择框", // 标题文字
    icon: "icon-calendar",
    decOpacity: false, options: {
      width: "100%", // 宽度
      defaultValue: timer, // 默认值，字符串 12:00:00
      rangeDefaultValue: [], // 默认值，字符串 12:00:00
      range: false, // 范围日期选择，为true则会显示两个时间选择框（同时defaultValue和placeholder要改成数组），
      showTime: false, // 是否显示时间选择器
      disabled: false, // 是否禁用
      hidden: false, // 是否隐藏，false显示，true隐藏
      newHidden: false, isShow: true,
      selected: '',
      selectedValue: '',
      clearable: false, // 是否显示清除按钮
      queryList: true, // 是否被查询列表获取
      print: false, // 是否可被打印
      placeholder: "请选择",
      rangePlaceholder: ["开始时间", "结束时间"],
      format: "YYYY-MM-DD" // 展示格式  （请按照这个规则写 YYYY-MM-DD HH:mm:ss，区分大小写）
    },
    labelCol: { span: 4 },
    wrapperCol: { span: 18 },
    model: "",
    key: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    blindComponentsNumber: [{ id: undefined, value: '1', bindValue: undefined }],
    type: "time", // 表单类型
    label: "时间选择框", // 标题文字
    icon: "icon-time",
    decOpacity: false, options: {
      readonly: false,
      width: "100%", // 宽度
      defaultValue: timer1, // 默认值，字符串 12:00:00
      disabled: false, // 是否禁用
      hidden: false, // 是否隐藏，false显示，true隐藏
      newHidden: false, isShow: true,
      selected: '',
      selectedValue: '',
      clearable: false, // 是否显示清除按钮
      queryList: true, // 是否被查询列表获取
      print: false, // 是否可被打印
      placeholder: "请选择",
      format: "HH:mm:ss" // 展示格式
    },
    labelCol: { span: 4 },
    wrapperCol: { span: 18 },
    model: "",
    key: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    blindComponentsNumber: [{ id: undefined, value: '1', bindValue: undefined }],
    type: "rate", // 表单类型
    label: "评分", // 标题文字
    icon: "icon-pingfen_moren",
    decOpacity: false, options: {
      defaultValue: 0,
      max: 5, // 最大值
      disabled: false, // 是否禁用
      hidden: false, // 是否隐藏，false显示，true隐藏
      newHidden: false, isShow: true,
      selected: '',
      selectedValue: '',
      queryList: true, // 是否被查询列表获取
      print: false, // 是否可被打印
      allowHalf: false // 是否允许半选
    },
    labelCol: { span: 4 },
    wrapperCol: { span: 18 },
    model: "",
    key: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    blindComponentsNumber: [{ id: undefined, value: '1', bindValue: undefined }],
    type: "slider", // 表单类型
    label: "滑动输入条", // 标题文字
    icon: "icon-menu",
    decOpacity: false, options: {
      width: "100%", // 宽度
      defaultValue: 0, // 默认值， 如果range为true的时候，则需要改成数组,如：[12,15]
      disabled: false, // 是否禁用
      hidden: false, // 是否隐藏，false显示，true隐藏
      newHidden: false, isShow: true,
      selected: '',
      selectedValue: '',
      min: 0, // 最小值
      max: 100, // 最大值
      step: 1, // 步长，取值必须大于 0，并且可被 (max - min) 整除
      queryList: true, // 是否被查询列表获取
      print: false, // 是否可被打印
      showInput: false // 是否显示输入框，range为true时，请勿开启
      // range: false // 双滑块模式
    },
    labelCol: { span: 4 },
    wrapperCol: { span: 18 },
    model: "",
    key: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    blindComponentsNumber: [{ id: undefined, value: '1', bindValue: undefined }],
    type: "uploadFile", // 表单类型
    label: "上传文件", // 标题文字
    icon: "icon-upload",
    decOpacity: false, options: {
      defaultValue: "[]",
      multiple: false,
      disabled: false,
      hidden: false, // 是否隐藏，false显示，true隐藏
      newHidden: false, isShow: true,
      selected: '',
      selectedValue: '',
      drag: false,
      downloadWay: "a",
      dynamicFun: "",
      width: "100%",
      limit: 3,
      data: "{}",
      fileName: "file",
      headers: {},
      action: "http://portal.goodcol.com/muses-gateway/api/upms/common/file/uploadFile",
      placeholder: "上传"
    },
    labelCol: { span: 4 },
    wrapperCol: { span: 18 },
    model: "",
    key: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    blindComponentsNumber: [{ id: undefined, value: '1', bindValue: undefined }],
    type: "uploadImg",
    label: "上传图片",
    icon: "icon-image",
    decOpacity: false, options: {
      defaultValue: "[]",
      multiple: false,
      hidden: false, // 是否隐藏，false显示，true隐藏
      newHidden: false, isShow: true,
      selected: '',
      selectedValue: '',
      disabled: false,
      width: "100%",
      data: "{}",
      limit: 3,
      placeholder: "上传",
      fileName: "image",
      headers: {},
      action: "http://cdn.kcz66.com/upload-img.txt",
      listType: "picture-card"
    },
    labelCol: { span: 4 },
    wrapperCol: { span: 18 },
    model: "",
    key: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    blindComponentsNumber: [{ id: undefined, value: '1', bindValue: undefined }],
    type: "treeSelect", // 表单类型
    label: "树选择器", // 标题文字
    icon: "icon-tree",
    decOpacity: false, options: {
      disabled: false, //是否禁用
      defaultValue: undefined, // 默认值
      multiple: true,
      hidden: false, // 是否隐藏，false显示，true隐藏
      newHidden: false, isShow: true,
      selected: '',
      selectedValue: '',
      clearable: false, // 是否显示清除按钮
      showSearch: false, // 是否显示搜索框，搜索选择的项的值，而不是文字
      treeCheckable: false,
      placeholder: "请选择",
      dynamicKey: "",
      dynamic: true,
      queryList: true, // 是否被查询列表获取
      print: false, // 是否可被打印
      replaceFields: {
        title: "orgName",
        value: 'id',
        children: 'children',
      },
      replaceFieldsStatic: {
        title: "label",
        value: 'value',
        children: 'children',
      },
      decOpacity: false, options: [
        { value: "254204307304542208", parentValue: "1", label: "移交安置科" },
        { value: "254204506861137920", parentValue: "1", label: "拥军优抚科" },
        { value: "256068110342365184", parentValue: "1", label: "财务科" },
        { value: "256725789754740736", parentValue: "1", label: "退役局分局" },
        { value: "266946191017975808", parentValue: "1", label: "111" },
        { value: "269025794091982848", parentValue: "1", label: "工作流总部" },
        { value: "261134132552433664", parentValue: "256725789754740736", label: "分局部门" },
        { value: "266946315890794496", parentValue: "256725789754740736", label: "3" },
        { value: "266946259926192128", parentValue: "266946191017975808", label: "222" },
        { value: "269025984135897088", parentValue: "269025794091982848", label: "工作流分部" },
        { value: "1", label: "张家港退役局" }
      ]
    },
    labelCol: { span: 4 },
    wrapperCol: { span: 18 },
    model: "",
    key: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    blindComponentsNumber: [{ id: undefined, value: '1', bindValue: undefined }],
    type: "cascader", // 表单类型
    label: "级联选择器", // 标题文字
    icon: "icon-guanlian",
    decOpacity: false, options: {
      disabled: false, //是否禁用
      hidden: false, // 是否隐藏，false显示，true隐藏
      newHidden: false, isShow: true,
      selected: '',
      selectedValue: '',
      fieldNames: {
        label: 'orgName',
        value: 'id',
        children: 'children'
      },
      defaultValue: undefined, // 默认值
      showSearch: false, // 是否显示搜索框，搜索选择的项的值，而不是文字
      placeholder: "请选择",
      clearable: false, // 是否显示清除按钮
      dynamicKey: "",
      dynamic: true,
      queryList: true, // 是否被查询列表获取
      print: false, // 是否可被打印
      decOpacity: false, options: [
        {
          value: "1",
          label: "选项1",
          children: [
            {
              value: "11",
              label: "选项11"
            }
          ]
        },
        {
          value: "2",
          label: "选项2",
          children: [
            {
              value: "22",
              label: "选项22"
            }
          ]
        }
      ]
    },
    labelCol: { span: 4 },
    wrapperCol: { span: 18 },
    model: "",
    key: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  // {
  //   type: "batch",
  //   label: "动态表格",
  //   icon: "icon-biaoge",
  //   list: [],
  //   decOpacity: false, options: {
  //     scrollY: 0,
  //     disabled: false,
  //     hidden: false, // 是否隐藏，false显示，true隐藏
  //     newHidden: false,isShow: true,
  //     selected: '',
  //     selectedValue: '',
  //     showLabel: false,
  //     hideSequence: false,
  //     width: "100%"
  //   },
  //   labelCol: { span: 4 },
  //   wrapperCol: { span: 18 },
  //   model: "",
  //   key: ""
  // },
  {
    blindComponentsNumber: [{ id: undefined, value: '1', bindValue: undefined }],
    type: "editor",
    label: "富文本",
    icon: "icon-LC_icon_edit_line_1",
    list: [],
    decOpacity: false, options: {
      height: 300,
      placeholder: "请输入",
      defaultValue: "",
      chinesization: true,
      hidden: false, // 是否隐藏，false显示，true隐藏
      newHidden: false, isShow: true,
      selected: '',
      selectedValue: '',
      disabled: false,
      showLabel: false,
      width: "100%"
    },
    labelCol: { span: 4 },
    wrapperCol: { span: 18 },
    model: "",
    key: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    type: "switch", // 表单类型
    label: "开关", // 标题文字
    icon: "icon-kaiguan3",
    decOpacity: false, options: {
      defaultValue: false, // 默认值 Boolean 类型
      hidden: false, // 是否隐藏，false显示，true隐藏
      newHidden: false, isShow: true,
      selected: '',
      selectedValue: '',
      queryList: true, // 是否被查询列表获取
      print: false, // 是否可被打印
      disabled: false // 是否禁用
    },
    labelCol: { span: 4 },
    wrapperCol: { span: 18 },
    model: "",
    key: "",
    rules: [
      {
        required: false,
        message: "必填项"
      }
    ]
  },
  {
    type: "alert",
    label: "警告提示",
    icon: "icon-zu",
    decOpacity: false, options: {
      type: "success",
      description: "",
      showIcon: false,
      banner: false,
      hidden: false, // 是否隐藏，false显示，true隐藏
      newHidden: false, isShow: true,
      selected: '',
      selectedValue: '',
      closable: false
    },
    key: ""
  },
  {
    type: "text",
    label: "文字",
    icon: "icon-zihao",
    decOpacity: false, options: {
      textAlign: "left",
      hidden: false, // 是否隐藏，false显示，true隐藏
      newHidden: false, isShow: true,
      selected: '',
      selectedValue: '',
      showRequiredMark: false
    },
    labelCol: { span: 4 },
    wrapperCol: { span: 18 },
    key: ""
  },
  {
    type: "html",
    label: "HTML",
    icon: "icon-ai-code",
    decOpacity: false, options: {
      hidden: false, // 是否隐藏，false显示，true隐藏
      newHidden: false, isShow: true,
      selected: '',
      selectedValue: '',
      defaultValue: "<strong>HTML</strong>"
    },
    labelCol: { span: 4 },
    wrapperCol: { span: 18 },
    key: ""
  },
  {
    type: "button", // 表单类型
    label: "按钮", // 标题文字
    icon: "icon-button-remove",
    decOpacity: false, options: {
      type: "primary",
      handle: "submit",
      dynamicFun: "",
      hidden: false, // 是否隐藏，false显示，true隐藏
      newHidden: false, isShow: true,
      selected: '',
      selectedValue: '',
      disabled: false // 是否禁用，false不禁用，true禁用
    },
    labelCol: { span: 4 },
    wrapperCol: { span: 18 },
    key: ""
  },
];

// 高级控件
// export const highList = [];

import { Alert, Select } from "ant-design-vue";

// 自定义组件
export const customComponents = {
  title: "自定义组件",
  list: [
    // {
    //   label: "测试",
    //   type: "jkjksdf",
    //   component: Alert,
    //   decOpacity: false, options: {
    //     multiple: false,
    //     disabled: false,
    //     width: "100%",
    //     data: "{}",
    //     limit: 3,
    //     placeholder: "上传",
    //     action: "",
    //     listType: "picture-card"
    //   },
    //   model: "",
    //   key: "",
    //   rules: [
    //     {
    //       required: false,
    //       message: "必填项"
    //     }
    //   ]
    // },
    {
      type: "posSelect", // 表单类型
      label: "岗位选择器", // 标题文字
      icon: "icon-RectangleCopy",
      blindComponentsNumber: [{ id: undefined, value: '1', bindValue: undefined }],
      blindComponentKey: [],
      addComponentKey: [],
      decOpacity: false, options: {
        isCondition: false,
        width: "100%", // 宽度
        defaultValue: undefined, // 下拉选框请使用undefined为默认值
        multiple: false, // 是否允许多选
        disabled: false, // 是否禁用
        clearable: false, // 是否显示清除按钮
        hidden: false, // 是否隐藏，false显示，true隐藏
        newHidden: false, isShow: true,
        selected: '',
        selectedValue: '',
        placeholder: "请选择岗位", // 默认提示文字
        dynamicKey: "posSelect",
        url: urlPos,
        dynamic: true,
        decOpacity: false, options: [
          // 下拉选择项配置
          {
            value: "1",
            label: "下拉框1"
          },
          {
            value: "2",
            label: "下拉框2"
          }
        ],
        queryList: true, // 是否被查询列表获取
        print: false, // 是否可被打印
        showSearch: true // 是否显示搜索框，搜索选择的项的值，而不是文字
      },
      labelCol: { span: 4 },
      wrapperCol: { span: 18 },
      model: "",
      key: "",
      rules: [
        {
          required: false,
          message: "必填项"
        }
      ]
    },
    {
      type: "customInput", // 表单类型
      label: "动态输入框", // 标题文字
      icon: "icon-write",
      blindComponentsNumber: [{ id: undefined, value: '1', bindValue: undefined }],
      decOpacity: false, options: {
        type: "text",
        width: "100%", // 宽度
        dynamicInputUrl: '',
        fieldName: '',
        defaultValue: "", // 默认值
        placeholder: "请输入", // 没有输入时，提示文字
        clearable: true,
        maxLength: null,
        hidden: false, // 是否隐藏，false显示，true隐藏
        newHidden: false, isShow: true,
        selected: '',
        selectedValue: '',
        disabled: false, // 是否禁用，false不禁用，true禁用
        queryList: true, // 是否被查询列表获取
        print: false // 是否可被打印
      },
      labelCol: { span: 4 },
      wrapperCol: { span: 18 },
      model: "", // 数据字段
      key: "",
      rules: [
        //验证规则
        {
          required: false, // 必须填写
          message: "必填项"
        }
      ]
    },
    {
      type: "rolSelect", // 表单类型
      label: "角色选择器", // 标题文字
      icon: "icon-guanlizhongxin",
      blindComponentKey: [],
      blindComponentsNumber: [{ id: undefined, value: '1', bindValue: undefined }],
      addComponentKey: [],
      decOpacity: false, options: {
        isCondition: false,
        width: "100%", // 宽度
        defaultValue: undefined, // 下拉选框请使用undefined为默认值
        multiple: false, // 是否允许多选
        disabled: false, // 是否禁用
        clearable: false, // 是否显示清除按钮
        hidden: false, // 是否隐藏，false显示，true隐藏
        newHidden: false, isShow: true,
        selected: '',
        selectedValue: '',
        placeholder: "请选择角色", // 默认提示文字
        dynamicKey: "rolSelect",
        url: urlRol,
        dynamic: true,
        decOpacity: false, options: [
          // 下拉选择项配置
          {
            value: "1",
            label: "下拉框1"
          },
          {
            value: "2",
            label: "下拉框2"
          }
        ],
        queryList: true, // 是否被查询列表获取
        print: false, // 是否可被打印
        showSearch: true // 是否显示搜索框，搜索选择的项的值，而不是文字
      },
      labelCol: { span: 4 },
      wrapperCol: { span: 18 },
      model: "",
      key: "",
      rules: [
        {
          required: false,
          message: "必填项"
        }
      ]
    },
    // {
    //   type: "groSelect", // 表单类型
    //   label: "群组选择器", // 标题文字
    //   icon: "icon-people-outline",
    //   blindComponentKey: [],
    //   blindComponentsNumber: [{ id: undefined, value: '1', bindValue: undefined }],
    //   addComponentKey: [],
    //   decOpacity: false, options: {
    //     isCondition: false,
    //     width: "100%", // 宽度
    //     defaultValue: undefined, // 下拉选框请使用undefined为默认值
    //     multiple: false, // 是否允许多选
    //     disabled: false, // 是否禁用
    //     clearable: false, // 是否显示清除按钮
    //     hidden: false, // 是否隐藏，false显示，true隐藏
    //     newHidden: false, isShow: true,
    //     selected: '',
    //     selectedValue: '',
    //     placeholder: "请选择角色", // 默认提示文字
    //     dynamicKey: "groSelect",
    //     url: urlGro,
    //     dynamic: true,
    //     decOpacity: false, options: [
    //       // 下拉选择项配置
    //       {
    //         value: "1",
    //         label: "下拉框1"
    //       },
    //       {
    //         value: "2",
    //         label: "下拉框2"
    //       }
    //     ],
    //     queryList: true, // 是否被查询列表获取
    //     print: false, // 是否可被打印
    //     showSearch: true // 是否显示搜索框，搜索选择的项的值，而不是文字
    //   },
    //   labelCol: { span: 4 },
    //   wrapperCol: { span: 18 },
    //   model: "",
    //   key: "",
    //   rules: [
    //     {
    //       required: false,
    //       message: "必填项"
    //     }
    //   ]
    // },
    {
      type: "orgSelect", // 表单类型
      label: "机构选择器", // 标题文字
      icon: "icon-zihuanjie2x",
      blindComponentKey: [],
      addComponentKey: [],
      blindComponentsNumber: [{ id: undefined, value: '1', bindValue: undefined }],
      decOpacity: false, options: {
        isCondition: false,
        width: "100%", // 宽度
        maxHeight: typeof setConfig !== 'undefined' && setConfig.orgSelect.maxHeight || '100vh',
        defaultValue: "[]", // 下拉选框请使用undefined为默认值
        multiple: typeof setConfig !== 'undefined' && setConfig.orgSelect.multiple || false, // 是否允许多选
        disabled: false, // 是否禁用
        clearable: false, // 是否显示清除按钮
        hidden: false, // 是否隐藏，false显示，true隐藏
        newHidden: false, isShow: true,
        selected: '',
        selectedValue: '',
        placeholder: "请选择机构", // 默认提示文字
        dynamicKey: "orgSelect",
        url: urlOrg,
        dynamic: true,
        treeDefaultExpandAll: typeof setConfig !== 'undefined' && setConfig.orgSelect.treeDefaultExpandAll || false,
        replaceFields: {
          key: typeof setConfig !== 'undefined' && setConfig.orgSelect.key || defaultConfig.orgSelect.key,
          title: typeof setConfig !== 'undefined' && setConfig.orgSelect.title || defaultConfig.orgSelect.title,
          value: typeof setConfig !== 'undefined' && setConfig.orgSelect.value || defaultConfig.orgSelect.value,
          children: typeof setConfig !== 'undefined' && setConfig.orgSelect.children || defaultConfig.orgSelect.children,
        },
        decOpacity: false, options: [
          // 下拉选择项配置
        ],
        queryList: true, // 是否被查询列表获取
        print: false, // 是否可被打印
        showSearch: true // 是否显示搜索框，搜索选择的项的值，而不是文字
      },
      labelCol: { span: 4 },
      wrapperCol: { span: 18 },
      model: "",
      key: "",
      rules: [
        {
          required: false,
          message: "必填项"
        }
      ]
    },
    // {
    //   type: "userSelectPanel", // 表单类型
    //   label: "用户选择器", // 标题文字
    //   icon: "icon-user-c",
    //   blindComponentsNumber: [{ id: undefined, value: '1', bindValue: undefined }],
    //   blindComponentKey: [],
    //   addComponentKey: [],
    //   decOpacity: false, options: {
    //     width: "100%", // 宽度
    //     maxHeight: typeof setConfig !== 'undefined' && setConfig.orgSelect.maxHeight || '100vh',
    //     columns: typeof setConfig !== 'undefined' && setConfig.userSelectPanel.columns || defaultConfig.userSelectPanel.columns,
    //     defaultValue: "[]", // 下拉选框请使用undefined为默认值
    //     multiple: typeof setConfig !== 'undefined' && setConfig.orgSelect.multiple || false, // 是否允许多选
    //     disabled: false, // 是否禁用
    //     clearable: false, // 是否显示清除按钮
    //     hidden: false, // 是否隐藏，false显示，true隐藏
    //     newHidden: false, isShow: true,
    //     selected: '',
    //     selectedValue: '',
    //     placeholder: "请选择用户", // 默认提示文字
    //     dynamicKey: "",
    //     url: urlOrg,
    //     dynamic: false,
    //     treeDefaultExpandAll: typeof setConfig !== 'undefined' && setConfig.orgSelect.treeDefaultExpandAll || false,
    //     replaceFields: {
    //       key: typeof setConfig !== 'undefined' && setConfig.orgSelect.key || defaultConfig.orgSelect.key,
    //       title: typeof setConfig !== 'undefined' && setConfig.orgSelect.title || defaultConfig.orgSelect.title,
    //       value: typeof setConfig !== 'undefined' && setConfig.orgSelect.value || defaultConfig.orgSelect.value,
    //       children: typeof setConfig !== 'undefined' && setConfig.orgSelect.children || defaultConfig.orgSelect.children,
    //     },
    //     decOpacity: false, options: [
    //       // 下拉选择项配置
    //     ],
    //     queryList: true, // 是否被查询列表获取
    //     print: false, // 是否可被打印
    //     showSearch: true // 是否显示搜索框，搜索选择的项的值，而不是文字
    //   },
    //   labelCol: { span: 4 },
    //   wrapperCol: { span: 18 },
    //   model: "",
    //   key: "",
    //   rules: [
    //     {
    //       required: false,
    //       message: "必填项"
    //     }
    //   ]
    // },
    {
      type: "linkmanSelectPanel", // 表单类型
      label: "联系人组件", // 标题文字
      btnText: '用户',
      icon: "icon-user-c",
      blindComponentsNumber: [{ id: undefined, value: '1', bindValue: undefined }],
      blindComponentKey: [],
      addComponentKey: [],
      decOpacity: false, options: {
        width: "100%", // 宽度
        maxHeight: typeof setConfig !== 'undefined' && setConfig.orgSelect.maxHeight || '100vh',
        columns: typeof setConfig !== 'undefined' && setConfig.userSelectPanel.columns || defaultConfig.userSelectPanel.columns,
        defaultValue: "[]", // 下拉选框请使用undefined为默认值
        multiple: typeof setConfig !== 'undefined' && setConfig.orgSelect.multiple || false, // 是否允许多选
        disabled: false, // 是否禁用
        clearable: false, // 是否显示清除按钮
        hidden: false, // 是否隐藏，false显示，true隐藏
        newHidden: false, isShow: true,
        selected: '',
        selectedValue: '',
        placeholder: "请选择用户", // 默认提示文字
        dynamicKey: "",
        url: urlOrg,
        dynamic: false,
        treeDefaultExpandAll: typeof setConfig !== 'undefined' && setConfig.orgSelect.treeDefaultExpandAll || false,
        replaceFields: {
          key: typeof setConfig !== 'undefined' && setConfig.orgSelect.key || defaultConfig.orgSelect.key,
          title: typeof setConfig !== 'undefined' && setConfig.orgSelect.title || defaultConfig.orgSelect.title,
          value: typeof setConfig !== 'undefined' && setConfig.orgSelect.value || defaultConfig.orgSelect.value,
          children: typeof setConfig !== 'undefined' && setConfig.orgSelect.children || defaultConfig.orgSelect.children,
        },
        decOpacity: false, options: [
          // 下拉选择项配置
        ],
        queryList: true, // 是否被查询列表获取
        print: false, // 是否可被打印
        showSearch: true // 是否显示搜索框，搜索选择的项的值，而不是文字
      },
      labelCol: { span: 4 },
      wrapperCol: { span: 18 },
      model: "",
      key: "",
      rules: [
        {
          required: false,
          message: "必填项"
        }
      ]
    },
    {
      type: "dynamicTable", // 表单类型
      label: "动态表格", // 标题文字
      icon: "icon-user-c",
      blindComponentKey: [],
      addComponentKey: [],
      blindComponentsNumber: [{ id: undefined, value: '1', bindValue: undefined }],
      dataTable: [

      ],
      decOpacity: false, options: {
        width: "100%", // 宽度
        maxHeight: typeof setConfig !== 'undefined' && setConfig.orgSelect.maxHeight || '100vh',
        dynamicTableKey: '',
        columns: [
          {
            title: "账号",
            dataIndex: "account",
            ellipsis: true,
          },
          {
            title: "姓名",
            dataIndex: "name",
            ellipsis: true,
          },
          {
            title: "性别",
            dataIndex: "sex",
            scopedSlots: { customRender: 'sex' },
            ellipsis: true,
          },
          {
            title: "手机号",
            dataIndex: "phone",
            ellipsis: true,
          },
        ],
        defaultValue: "[]", // 下拉选框请使用undefined为默认值
        multiple: typeof setConfig !== 'undefined' && setConfig.orgSelect.multiple || false, // 是否允许多选
        disabled: false, // 是否禁用
        clearable: false, // 是否显示清除按钮
        hidden: false, // 是否隐藏，false显示，true隐藏
        newHidden: false, isShow: true,
        selected: '',
        selectedValue: '',
        placeholder: "请选择用户", // 默认提示文字
        dynamicKey: "",
        url: urlOrg,
        dynamic: false,
        treeDefaultExpandAll: typeof setConfig !== 'undefined' && setConfig.orgSelect.treeDefaultExpandAll || false,
        replaceFields: {
          key: typeof setConfig !== 'undefined' && setConfig.orgSelect.key || defaultConfig.orgSelect.key,
          title: typeof setConfig !== 'undefined' && setConfig.orgSelect.title || defaultConfig.orgSelect.title,
          value: typeof setConfig !== 'undefined' && setConfig.orgSelect.value || defaultConfig.orgSelect.value,
          children: typeof setConfig !== 'undefined' && setConfig.orgSelect.children || defaultConfig.orgSelect.children,
        },
        decOpacity: false, options: [
          // 下拉选择项配置
        ],
        queryList: true, // 是否被查询列表获取
        print: false, // 是否可被打印
        showSearch: true // 是否显示搜索框，搜索选择的项的值，而不是文字
      },
      labelCol: { span: 4 },
      wrapperCol: { span: 16 },
      model: "",
      key: "",
      rules: [
        {
          required: false,
          message: "必填项"
        }
      ]
    },
  ]
};
// window.$customComponentList = customComponents.list;

// 布局控件
export const layoutList = [
  {
    type: "divider",
    label: "分割线",
    icon: "icon-fengexian",
    decOpacity: false, options: {
      orientation: "left"
    },
    key: "",
    model: ""
  },
  {
    type: "card",
    label: "卡片布局",
    icon: "icon-qiapian",
    list: [],
    key: "",
    model: ""
  },
  {
    type: "grid",
    label: "栅格布局",
    icon: "icon-zhage",
    columns: [
      {
        span: 12,
        list: []
      },
      {
        span: 12,
        list: []
      }
    ],
    decOpacity: false, options: {
      gutter: 0
    },
    key: "",
    model: ""
  },
  {
    type: "table",
    label: "表格布局",
    icon: "icon-biaoge",
    trs: [
      {
        tds: [
          {
            colspan: 1,
            rowspan: 1,
            list: []
          },
          {
            colspan: 1,
            rowspan: 1,
            list: []
          }
        ]
      },
      {
        tds: [
          {
            colspan: 1,
            rowspan: 1,
            list: []
          },
          {
            colspan: 1,
            rowspan: 1,
            list: []
          }
        ]
      }
    ],
    decOpacity: false, options: {
      width: "100%",
      bordered: true,
      bright: false,
      small: true,
      customStyle: ""
    },
    key: "",
    model: ""
  }
];
